<?php

namespace App\Http\Utils\FeedFetch;

use Illuminate\Support\Facades\Log;

interface FeedFetchBasic
{
    function getInfos();
}
